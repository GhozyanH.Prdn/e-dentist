-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 17 Bulan Mei 2020 pada 16.44
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `e-dentist`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `diagnosa`
--

CREATE TABLE `diagnosa` (
  `id` int(11) NOT NULL,
  `kode` varchar(100) NOT NULL,
  `nama` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `diagnosa`
--

INSERT INTO `diagnosa` (`id`, `kode`, `nama`) VALUES
(1, 'Gg76', 'Cabut Gigi');

-- --------------------------------------------------------

--
-- Struktur dari tabel `dosen`
--

CREATE TABLE `dosen` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `nip` varchar(18) DEFAULT NULL,
  `tempat` varchar(100) DEFAULT NULL,
  `ttl` varchar(100) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `no_telepon` varchar(11) DEFAULT NULL,
  `gambar` varchar(100) DEFAULT NULL,
  `tanggal` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `dosen`
--

INSERT INTO `dosen` (`id`, `nama`, `password`, `level`, `nip`, `tempat`, `ttl`, `alamat`, `no_telepon`, `gambar`, `tanggal`) VALUES
(1, 'Nur Isnaini', '827ccb0eea8a706c4c34a16891f84e7b', 1, '76218038', 'Surabaya', '2020-05-15', 'Banjar Sugihan', '01918178811', 'default.jpg', '2020-05-15'),
(4, 'Bu Pipit', '827ccb0eea8a706c4c34a16891f84e7b', 2, '1234567', 'Surabaya', '2020-05-15', 'Jogorogo Ngawi Jawa Timur Indoensia', '08127333854', 'default.jpg', '2020-05-17'),
(5, 'Bu Pipit', '827ccb0eea8a706c4c34a16891f84e7b', 2, '199204292019022002', 'Surabaya', '2020-05-16', 'Jogorogo Ngawi Jawa Timur Indonesia', '08127333854', 'default.jpg', '2020-05-17');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kode_gigi_anak`
--

CREATE TABLE `kode_gigi_anak` (
  `id` int(11) NOT NULL,
  `image` varchar(10) NOT NULL,
  `kode` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kode_gigi_anak`
--

INSERT INTO `kode_gigi_anak` (`id`, `image`, `kode`) VALUES
(1, '51.jpeg', 51),
(2, '52.jpeg', 52),
(3, '53.jpeg', 53),
(4, '54.jpeg', 54),
(5, '55.jpeg', 55),
(6, '61.jpeg', 61),
(7, '62.jpeg', 62),
(8, '63.jpeg', 63),
(9, '64.jpeg', 64),
(10, '65.jpeg', 65),
(11, '71.jpeg', 71),
(12, '72.jpeg', 72),
(13, '73.jpeg', 73),
(14, '74.jpeg', 74),
(15, '75.jpeg', 75),
(16, '81.jpeg', 81),
(17, '82.jpeg', 82),
(18, '83.jpeg', 83),
(19, '84.jpeg', 84),
(20, '85.jpeg', 85);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kode_gigi_dewasa`
--

CREATE TABLE `kode_gigi_dewasa` (
  `id` int(11) NOT NULL,
  `image` varchar(10) NOT NULL,
  `kode` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kode_gigi_dewasa`
--

INSERT INTO `kode_gigi_dewasa` (`id`, `image`, `kode`) VALUES
(1, '11.jpeg', 11),
(2, '12.jpeg', 12),
(3, '13.jpeg', 13),
(4, '14.jpeg', 14),
(5, '15.jpeg', 15),
(6, '16.jpeg', 16),
(7, '17.jpeg', 17),
(8, '18.jpeg', 18),
(9, '21.jpeg', 21),
(10, '22.jpeg', 22),
(11, '23.jpeg', 23),
(12, '24.jpeg', 24),
(13, '25.jpeg', 25),
(14, '26.jpeg', 26),
(15, '27.jpeg', 27),
(16, '28.jpeg', 28),
(17, '31.jpeg', 31),
(18, '32.jpeg', 32),
(19, '33.jpeg', 33),
(20, '34.jpeg', 34),
(21, '35.jpeg', 35),
(22, '36.jpeg', 36),
(23, '37.jpeg', 37),
(24, '38.jpeg', 38),
(25, '41.jpeg', 41),
(26, '42.jpeg', 42),
(27, '43.jpeg', 43),
(28, '44.jpeg', 44),
(29, '45.jpeg', 45),
(32, '46.jpeg', 46),
(33, '47.jpeg', 47),
(34, '48.jpeg', 48);

-- --------------------------------------------------------

--
-- Struktur dari tabel `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `password` varchar(100) NOT NULL,
  `nim` varchar(100) DEFAULT NULL,
  `tempat` varchar(100) DEFAULT NULL,
  `ttl` varchar(100) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `no_telepon` varchar(11) DEFAULT NULL,
  `jenjang` varchar(100) DEFAULT NULL,
  `gambar` varchar(100) DEFAULT NULL,
  `tanggal` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `mahasiswa`
--

INSERT INTO `mahasiswa` (`id`, `nama`, `password`, `nim`, `tempat`, `ttl`, `alamat`, `no_telepon`, `jenjang`, `gambar`, `tanggal`) VALUES
(3, 'Ghozyan Hilman Pradana', '827ccb0eea8a706c4c34a16891f84e7b', 'H06216009', 'Surabaya', '2020-05-15', 'Jogorogo Ngawi Jawa Timur Indoensia', '08127333854', 'D3', 'default.jpg', '2020-05-17'),
(4, 'Nur Isnaini', '827ccb0eea8a706c4c34a16891f84e7b', 'P27825117030', 'Surabaya', '2020-05-16', 'Jogorogo Ngawi Jawa Timur Indoensia', '08127338544', 'D3', 'default.jpg', '2020-05-16'),
(5, NULL, '827ccb0eea8a706c4c34a16891f84e7b', 'D4', NULL, NULL, NULL, NULL, 'default.jpg', '2020-05-16', '0000-00-00'),
(6, 'Ghozyan', '827ccb0eea8a706c4c34a16891f84e7b', 'H06216010', 'Surabaya', '2020-05-16', 'Jogorogo Ngawi Jawa Timur Indoensia', '08127338544', 'D4', 'default.jpg', '2020-05-16');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pasien`
--

CREATE TABLE `pasien` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `kode` varchar(15) NOT NULL,
  `tempat` varchar(100) NOT NULL,
  `ttl` varchar(100) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `kelamin` varchar(100) NOT NULL,
  `no_telepon` varchar(100) NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pasien`
--

INSERT INTO `pasien` (`id`, `nama`, `kode`, `tempat`, `ttl`, `alamat`, `kelamin`, `no_telepon`, `tanggal`) VALUES
(4, 'Nur Isnaini', 'JKG3001', 'Surabaya', '2020-05-17', 'Jogorogo Ngawi Jawa Timur Indonesia', 'Laki-Laki', '08127338544', '2020-05-17');

-- --------------------------------------------------------

--
-- Struktur dari tabel `perawatan`
--

CREATE TABLE `perawatan` (
  `id` int(11) NOT NULL,
  `id_mahasiswa` int(11) NOT NULL,
  `id_dosen` int(11) NOT NULL,
  `id_pasien` int(11) NOT NULL,
  `diagnosa` varchar(100) NOT NULL,
  `keluhan` varchar(100) NOT NULL,
  `kode_gigi` varchar(100) NOT NULL,
  `tindakan` varchar(100) NOT NULL,
  `tanggal` date NOT NULL,
  `bulan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `perawatan`
--

INSERT INTO `perawatan` (`id`, `id_mahasiswa`, `id_dosen`, `id_pasien`, `diagnosa`, `keluhan`, `kode_gigi`, `tindakan`, `tanggal`, `bulan`) VALUES
(1, 3, 4, 3, '1', 'Gigi Sakit', '13', 'Operasi Ringan', '2020-05-15', 'Mei'),
(2, 3, 4, 3, '1', 'Gigi Sakit', '15', 'Operasi Ringan', '2020-05-15', 'Mei'),
(3, 3, 4, 3, '1', 'Gigi Sakit', '48', 'Operasi Ringan', '2020-05-16', 'Mei'),
(4, 3, 4, 3, '1', 'Gigi Sakit', '44', 'Operasi Ringan', '2020-06-16', 'Juni'),
(5, 6, 4, 3, '1', 'Gigi Sakit', '25', 'Operasi Ringan', '2020-05-16', 'Juni'),
(6, 3, 4, 4, '1', 'Gigi Sakit', '16', 'Operasi Ringan', '2020-05-17', 'Mei');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `diagnosa`
--
ALTER TABLE `diagnosa`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `dosen`
--
ALTER TABLE `dosen`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nip` (`nip`);

--
-- Indeks untuk tabel `kode_gigi_anak`
--
ALTER TABLE `kode_gigi_anak`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kode_gigi_dewasa`
--
ALTER TABLE `kode_gigi_dewasa`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pasien`
--
ALTER TABLE `pasien`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `perawatan`
--
ALTER TABLE `perawatan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_mahasiswa` (`id_mahasiswa`),
  ADD KEY `id_mahasiswa_2` (`id_mahasiswa`),
  ADD KEY `id_dosen` (`id_dosen`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `diagnosa`
--
ALTER TABLE `diagnosa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `dosen`
--
ALTER TABLE `dosen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `mahasiswa`
--
ALTER TABLE `mahasiswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `pasien`
--
ALTER TABLE `pasien`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `perawatan`
--
ALTER TABLE `perawatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
