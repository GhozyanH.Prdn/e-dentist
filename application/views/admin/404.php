<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
 <title>E-JKG</title>


  <!-- Custom fonts for this template-->
  <link rel="icon" type="image/png" href="<?php echo base_url().'assets/login1/images/icons/gigi.png'?>"/>
  <link href="<?php echo base_url().'assets/modal.css'?>" rel="stylesheet">
  <link href="<?php echo base_url().'assets/backend/vendor/fontawesome-free/css/all.min.css'?>" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?php echo base_url().'assets/backend/css/sb-admin-2.min.css'?>" rel="stylesheet">
  <script type="text/javascript" src="<?php echo base_url().'assets/js/jquery-new.js'?>"></script>
  
  

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">


    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

        </nav>
        <!-- End of Topbar -->

		<div class="container-fluid">

          <!-- 404 Error Text -->
          <div class="text-center">
            <div class="error mx-auto" data-text="404">404</div>
            <p class="lead text-gray-800 mb-5">Page Not Found</p>
            <h2 class="text-gray-500 mb-0">Anda tidak berhak mengakses halaman ini !!</h2>
            <br>
            <i class="fa fa-exclamation-triangle fa-10x" aria-hidden="true"></i>
          </div>

        </div>



    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->
</div>
  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url().'assets/backend/vendor/jquery/jquery.min.js'?>"></script>
  <script src="<?php echo base_url().'assets/backend/vendor/jquery/jquery.js'?>"></script>
  <script src="<?php echo base_url().'assets/backend/vendor/bootstrap/js/bootstrap.bundle.min.js'?>"></script>
  
  <!-- Core plugin JavaScript-->
  <script src="<?php echo base_url().'assets/backend/vendor/jquery-easing/jquery.easing.min.js'?>"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?php echo base_url().'assets/backend/js/sb-admin-2.min.js'?>"></script>
  <!-- Page level plugins -->


  


</body>

</html>
